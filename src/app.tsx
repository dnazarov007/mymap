import React from 'react';
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';
import './leaflet.css'

const App = () => {
    return(
       
            <MapContainer center={[55.7575, 48.7364]} zoom={13} scrollWheelZoom={false} style={{height: "500px"}}>
                <TileLayer
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                <Marker position={[55.7575, 48.7364]}>
                    <Popup>
                    A pretty CSS3 popup. <br /> Easily customizable.
                    </Popup>
                </Marker>
            </MapContainer>
  
    )
}
export default App;

